var translations = {
	"Enter new data": "Voer nieuwe data in",
	"Date:": "Datum:",
	"Electricity:": "Elektriciteit",
	"Gas:": "Gas:",
	"Save": "Opslaan",
	"Electricity consumption in kWh": "Meterstand elektriciteit (kWh)",
	"Gas consumption in m³": "Meterstand gas (m³)",
	"Date": "Datum",
	"Electicity (kWh)": "Elektriciteit (kWh)",
	"Gas (m³)": "Gas (m³)"
}
