var webdb = {};
webdb.db = null;

webdb.open = function(){
	var dbSize = 10 * 1024 * 1024;
	webdb.db = openDatabase("energyapp", "1.0", "Energy consumption of the user", dbSize);
}

webdb.createTable = function(){
	webdb.db.transaction(function(tx){
		tx.executeSql("CREATE TABLE IF NOT EXISTS meters(id INTEGER PRIMARY KEY, date VARCHAR(10) UNIQUE, electricity INTEGER, gas INTEGER)", []);
	});
}

webdb.dropTable = function(){
	webdb.db.transaction(function(tx){
		tx.executeSql("DROP TABLE meters", []);
	});
}

webdb.insertValue = function(electricity, gas, date, onSuccess, onError){
	if (!date){
		return //var date = moment().format("YYYY-MM-DD");
	}
	webdb.db.transaction(function(tx){
		
		webdb.onSuccess = onSuccess;
		webdb.onError = onError;
		tx.executeSql("REPLACE INTO meters(date, electricity, gas) VALUES (?, ?, ?)", [date, electricity, gas], onSuccess, onError);	
	});
}

webdb.onSuccess = function(tx, r){
	console.log("success");
}

webdb.onError = function(tx, e){
	console.log(e);
}

webdb.getMeterData = function(date, render) {
	webdb.db.transaction(function(tx) {
    	tx.executeSql("SELECT * FROM meters", [], function(tx, rs){ 
    		var data = [];
			for (var i=0; i < rs.rows.length; i++) {
				data.push(rs.rows.item(i));
	  		}
	  		render(data);
		});
	});
}

webdb.open();
//webdb.dropTable();
webdb.createTable();
