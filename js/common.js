// Set constants
var DATE_FORMAT = _("DD-MM-YYYY");
var DATE_FORMAT_MOBISCROLL = _("dd-mm-yy");
var DATE_ORDER = _("ddmmy");


$(document).on("pageinit", function (event) {
	// Global settings
	$(".footer").load('footer.html', function(){
    	$(".footer").trigger("create");
    });
    
    $.mobiscroll.setDefaults({"theme": "jqm", "dateFormat": DATE_FORMAT_MOBISCROLL, "dateOrder": DATE_ORDER});
    $(".date-input").mobiscroll().date();
  	
});

